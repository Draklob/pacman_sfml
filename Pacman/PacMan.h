/****************************************************************
* Filename: PacMan.h part of the project Pacman
* Copyright (c) 2018 - Javier Barreiro
****************************************************************/

#ifndef PACMAN_PACMAN_H
#define PACMAN_PACMAN_H

#include "Character.h"
#include "Animator.h"

class PacMan : public Character
{

public:
	PacMan(sf::Texture& texture);

	void die();

	bool isDead() const;
	bool isDying() const;

	void update( sf::Time delta);

	void reset();

private:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	
	sf::Sprite m_sprite;
	bool m_isDead;
	bool m_isDying;

	Animator m_runAnimator;
	Animator m_dieAnimator;
};

#endif // PACMAN_PACMAN_H



