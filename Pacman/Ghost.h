﻿/****************************************************************
 * Filename: Ghost.h part of the project Pacman
 * Copyright (c) 2018 - Javier Barreiro
 ****************************************************************/

#ifndef GHOST_H
#define GHOST_H
#include "Character.h"
#include "Animator.h"
#include "PacMan.h"

class Ghost : public Character
{
public:
	Ghost(sf::Texture& texture, PacMan* pacman);

	enum State
	{
		Strong,
		Weak
	};

	void setWeak( sf::Time duration );
	bool isWeak() const;

	void update( sf::Time delta);

protected:
	void changeDirection() override;

private:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	PacMan* m_pacMan;
	sf::Sprite m_sprite;
	sf::Time m_weaknessDuration;
	bool m_isWeak;

	Animator m_strongAnimator;
	Animator m_weakAnimator;
};

#endif // GHOST_H