﻿/****************************************************************
 * Filename: Animator.cpp part of the project Pacman
 * Copyright (c) 2018 - Javier Barreiro
 ****************************************************************/

#include "Animator.h"

Animator::Animator()
:m_currentFrame(0), m_isPlaying( false), m_duration(sf::Time::Zero), m_loop(false)
{
}

void Animator::addFrame(sf::IntRect frame)
{
	m_frames.push_back(frame);
}

bool Animator::isPlaying() const
{
	return m_isPlaying;
}

void Animator::play(sf::Time duration, bool loop)
{
	m_isPlaying = true;
	m_duration = duration;
	m_loop = loop;
}

void Animator::stop()
{
	this->m_isPlaying = false;
}


void Animator::update(sf::Time delta)
{
	if( !isPlaying())
		return;

	static sf::Time timeBuffer = sf::Time::Zero;
	timeBuffer += delta;

	while( timeBuffer > m_duration)
	{
		// reset time, but keep the remainder
		timeBuffer = sf::microseconds(timeBuffer.asMicroseconds() % m_duration.asMicroseconds());

		// get next Frame index
		if (m_currentFrame + 1 < m_frames.size())
			m_currentFrame++;
		else
		{
			// animation has ended
			m_currentFrame = 0; // reset to start
			if( !m_loop)
				m_isPlaying = false;
		}
	}
}

void Animator::animate(sf::Sprite& sprite)
{
	sprite.setTextureRect( m_frames[m_currentFrame]);
}