/****************************************************************
* Filename: PacMan.cpp part of the project Pacman
* Copyright (c) 2018 - Javier Barreiro
****************************************************************/

#include "PacMan.h"
#include <iostream>

PacMan::PacMan(sf::Texture& texture)
:m_sprite( texture), m_isDead(false), m_isDying(false)
{
	setOrigin(20,20);

	m_runAnimator.addFrame(sf::IntRect(0, 32, 40, 40));
	m_runAnimator.addFrame(sf::IntRect(0, 72, 40, 40));

	m_dieAnimator.addFrame(sf::IntRect(0, 32, 40, 40));
	m_dieAnimator.addFrame(sf::IntRect(0, 72, 40, 40));
	m_dieAnimator.addFrame(sf::IntRect(0, 112, 40, 40));
	m_dieAnimator.addFrame(sf::IntRect(40, 112, 40, 40));
	m_dieAnimator.addFrame(sf::IntRect(80, 112, 40, 40));
	m_dieAnimator.addFrame(sf::IntRect(120, 112, 40, 40));
	m_dieAnimator.addFrame(sf::IntRect(160, 112, 40, 40));

	m_runAnimator.play(sf::seconds(0.2), true);
}

void PacMan::die()
{
	if(!m_isDying)
	{
		m_runAnimator.stop();

		m_dieAnimator.play(sf::seconds(0.15), false);
		m_isDying = true;
	}
}

void PacMan::reset()
{
	m_isDying = false;
	m_isDead = false;

	m_runAnimator.play(sf::seconds(0.2), true);
	m_runAnimator.animate(m_sprite);
}


bool PacMan::isDead() const
{
	return m_isDead;
}

bool PacMan::isDying() const
{
	return m_isDying;
}

void PacMan::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	if(!m_isDead)
		target.draw( m_sprite, states);
}

void PacMan::update(sf::Time delta)
{
//	std::cout << std::boolalpha;
//	std::cout << m_dieAnimator.isPlaying() << std::endl ;
	//std::cout << m_isDying << std::endl ;

	if(!m_isDead && !m_isDying)
	{
		m_runAnimator.update(delta);
		m_runAnimator.animate(m_sprite);
	}
	else
	{
		m_dieAnimator.update(delta);
		m_dieAnimator.animate(m_sprite);

		if( !m_dieAnimator.isPlaying())
		{
			m_isDying = false;
			m_isDead = true;
		}
	}
	Character::update(delta);
}
