/****************************************************************
* Filename: Game.h part of the project Pacman
* Copyright (c) 2018 - Javier Barreiro
****************************************************************/

#ifndef PACMAN_GAME_H
#define PACMAN_GAME_H

#include <SFML/Graphics.hpp>
#include <array>

#include "GameState.h"

class Game
{
public:
	Game();
	~Game();

	// Runs the game.
	void run();

	// Gets
	sf::Font& getFont();
	sf::Texture& getLogo();
	sf::Texture& getTexture();

	sf::RenderWindow& getWindow();

	// Changes game state
	void changeGameState( GameState::State gameState);

private:
	// Window where everything is going to be rendering.
	sf::RenderWindow m_window;
	// Game State
	GameState* m_currentState;

	// Game States
	std::array<GameState*, GameState::Count> m_gameStates;

	// Font for the game
	sf::Font m_font;
	sf::Texture m_logo;
	sf::Texture m_texture;
};


#endif // !PACMAN_GAME_H