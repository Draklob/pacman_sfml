﻿/****************************************************************
* Filename: Character.h part of the project Pacman
* Copyright (c) 2018 - Javier Barreiro
****************************************************************/

#include "Character.h"

Character::Character()
:m_speed(25.f), m_maze(nullptr), m_currentDirection(1,0), m_nextDirection(0,0), m_previousIntersection(0,0)
{
}

void Character::setMaze(Maze* maze)
{
	m_maze = maze;
}


void Character::update(sf::Time delta)
{
	sf::Vector2f pixelPosition = getPosition();

	// We express the pixel in seconds after 1 frame
	float pixelTraveled = getSpeed() * delta.asSeconds();

	sf::Vector2f nextPixelPosition = pixelPosition + sf::Vector2f(m_currentDirection) * pixelTraveled;
	setPosition(nextPixelPosition);

	sf::Vector2i cellPosition = m_maze->mapPixelToCell(pixelPosition);
	
	sf::Vector2f offset;
	offset.x = fmod(pixelPosition.x, 32) - 16;
	offset.y = fmod(pixelPosition.y, 32) - 16;

	if( m_maze->isWall( cellPosition + m_currentDirection))
	{
		// Check if there is a collision against the wall preventing one character from passing through a wall
		if ((m_currentDirection.x == 1 && offset.x > 0) ||
			(m_currentDirection.x == -1 && offset.x < 0) ||
			(m_currentDirection.y == 1 && offset.y > 0) ||
			(m_currentDirection.y == -1 && offset.y < 0))
		{
			setPosition(m_maze->mapCellToPixel(cellPosition));
		}
	}

	// Next direction. When the direcction is different from the current one, we check this to do the next movement.
	if (!m_maze->isWall(cellPosition + m_nextDirection) && m_currentDirection != m_nextDirection)
	{
		if ((!m_currentDirection.y && (offset.x > -2 && offset.x < 2)) ||
			(!m_currentDirection.x && (offset.y > -2 && offset.y < 2)))
		{
			// We need to recenter the character in the middle of the cell
			setPosition(m_maze->mapCellToPixel(cellPosition));
			m_currentDirection = m_nextDirection;

			// We change the transformation to sprite to fix the animation where he's going .
			if (m_currentDirection == sf::Vector2i(1, 0))
			{
				setRotation(0);
				setScale(-1, 1);
			}
			else if (m_currentDirection == sf::Vector2i(0, 1))
			{
				setRotation(90);
				setScale(-1, 1);
			}
			else if (m_currentDirection == sf::Vector2i(-1, 0))
			{
				setRotation(0);
				setScale(1, 1);
			}
			else if (m_currentDirection == sf::Vector2i(0, -1))
			{
				setRotation(90);
				setScale(1, 1);
			}
		}
	}

	static sf::Vector2i directions[4] = {
		sf::Vector2i(1, 0),
		sf::Vector2i(0, 1),
		sf::Vector2i(-1, 0),
		sf::Vector2i(0, -1)
	};

	if (cellPosition != m_previousIntersection)
	{
		if ((!m_currentDirection.y && (offset.x > -2 && offset.x < 2)) ||
			(!m_currentDirection.x && (offset.y > -2 && offset.y < 2)))
		{
			std::array<bool, 4> availableDirections;

			int i = 0;
			for (auto direction : directions)
			{
				availableDirections[i] = m_maze->isWall(cellPosition + direction);
				i++;
			}

			if (m_availableDirections != availableDirections)
			{

				m_previousIntersection = cellPosition;
				m_availableDirections = availableDirections;

				changeDirection();
			}
		}
	}
}

void Character::setDirection(sf::Vector2i direction)
{
	m_nextDirection = direction;
}

sf::Vector2i Character::getDirection() const
{
	return m_currentDirection;
}

void Character::setSpeed(float speed)
{
	m_speed = speed;
}

float Character::getSpeed() const
{
	return m_speed;
}

bool Character::willMove() const
{
	return !m_maze->isWall( m_previousIntersection + m_nextDirection);
}

sf::FloatRect Character::getColissionBox() const
{
	sf::FloatRect bounds(3, 3, 34, 34);
	return getTransform().transformRect(bounds);
}
