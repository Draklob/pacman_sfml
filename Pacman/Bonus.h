/****************************************************************
* Filename: Bonus.h part of the project Pacman
* Copyright (c) 2018 - Javier Barreiro
****************************************************************/

#ifndef PACMAN_BONUS_H
#define PACMAN_BONUS_H
#include <SFML/Graphics.hpp>

class Bonus : public sf::Drawable, public sf::Transformable
{
public:
	enum Fruit
	{
		Banana,
		Apple,
		Cherry
	};

public:
	Bonus(sf::Texture& texture);

	void setFruit( Fruit fruit);

private:
	sf::Sprite m_sprite;
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};

#endif //PACMAN_BONUS_H