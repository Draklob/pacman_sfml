/****************************************************************
* Filename: Dot.h part of the project Pacman
* Copyright (c) 2018 - Javier Barreiro
****************************************************************/

#ifndef PACMAN_DOT_H
#define PACMAN_DOT_H

#include <SFML/Graphics.hpp>

sf::CircleShape getDot();
sf::CircleShape getSuperDot();

#endif // PACMAN_DOT_H