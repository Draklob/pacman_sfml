﻿/****************************************************************
 * Filename: "Maze.h" is part of the project Pacman
 * Copyright (c) 2018 - Javier Barreiro
 ****************************************************************/

#ifndef MAZE_H
#define MAZE_H
#include <SFML/Graphics.hpp>

class Maze : public sf::Drawable
{
public:
	Maze(sf::Texture& texture);
	void loadLevel( std::string name);

	sf::Vector2i getPacmanPosition() const;
	std::vector<sf::Vector2i> getGhostPositions() const;

	inline size_t positionToIndex(sf::Vector2i position) const;
	inline sf::Vector2i indexToPosition(size_t index ) const;

	sf::Vector2i mapPixelToCell(sf::Vector2f pixel) const;
	sf::Vector2f mapCellToPixel(sf::Vector2i cell) const;

	bool isWall(sf::Vector2i position) const;
	bool isDot(sf::Vector2i position) const;
	bool isSuperDot(sf::Vector2i position) const;
	void pickObject(sf::Vector2i position);

	bool isBonus(sf::Vector2i position) const;

	sf::Vector2i getSize() const;

	int getRemainingDots() const;
	
private:

	enum CellData
	{
		Empty,
		Wall,
		Dot,
		SuperDot,
		Bonus
	};
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	sf::Vector2i m_mazeSize;
	std::vector<CellData> m_mazeData;
	sf::Vector2i m_pacmanPosition;
	std::vector<sf::Vector2i> m_ghostPositions;

	sf::RenderTexture m_renderTexture;
	sf::Texture& m_texture;
};

#endif // MAZE_H