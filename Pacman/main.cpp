/****************************************************************
* Filename: main.cpp part of the project Pacman
* Copyright (c) 2018 - Javier Barreiro
****************************************************************/

#include <SFML/Graphics.hpp>
#include <iostream>

#include "Game.h"

int main()
{
	Game pacman;
	pacman.run();

	return EXIT_SUCCESS;
}

