/****************************************************************
* Filename: Bonus.cpp part of the project Pacman
* Copyright (c) 2018 - Javier Barreiro
****************************************************************/

#include "Bonus.h"

Bonus::Bonus(sf::Texture& texture)
:m_sprite(texture)
{
	m_sprite.setOrigin(15,15);
	setFruit(Banana);
}

void Bonus::setFruit(Fruit fruit)
{
	if( fruit == Banana )
		m_sprite.setTextureRect(sf::IntRect(32,0,30,30));
	else if ( fruit == Apple )
		m_sprite.setTextureRect(sf::IntRect(32 + 30, 0, 30, 30));
	else if ( fruit == Cherry )
			m_sprite.setTextureRect(sf::IntRect(32 + 60, 0, 30, 30));

}

void Bonus::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_sprite, states);
}
