﻿/****************************************************************
 * Filename: Animator.h part of the project Pacman
 * Copyright (c) 2018 - Javier Barreiro
 ****************************************************************/

#ifndef ANIMATOR_H
#define ANIMATOR_H
#include <SFML/Graphics.hpp>

class Animator {
public:
	Animator();

	// Add rectangle data for every frame
	void addFrame( sf::IntRect frame );

	void play( sf::Time duration, bool loop );
	void stop();
	bool isPlaying() const;

	void update( sf::Time delta);
	void animate( sf::Sprite& sprite);

private:
	std::vector<sf::IntRect> m_frames;

	bool m_isPlaying;
	sf::Time m_duration;
	bool m_loop;

	// save the index of the frame currently played
	unsigned int m_currentFrame;
	
};

#endif // ANIMATOR_H